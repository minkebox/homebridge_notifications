#! /bin/sh

ALL_ACCESSORIES="$(cat /app/accessories1.conf),$(cat /app/accessories2.conf)"
ALL_ACCESSORIES=$(echo ${ALL_ACCESSORIES} |  sed -e "s/,,*/,/g" -e "s/^,//" -e "s/,$//")

cat /app/config.json | sed \
  -e "s/{{BRIDGE_USERNAME}}/${BRIDGE_USERNAME}/g" \
  -e "s/{{BRIDGE_PIN}}/${BRIDGE_PIN}/g" \
  -e "s/{{BRIDGE_SETUPID}}/${BRIDGE_SETUPID}/g" \
  -e "s|{{ALL_ACCESSORIES}}|${ALL_ACCESSORIES}|g" \
  > /app/homebridge/config.json
