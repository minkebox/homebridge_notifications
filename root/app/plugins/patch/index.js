// Patch the body method so we can augment the messages

const TimeInWords = require('time-in-words');

const ConfigParser = require("homebridge-http-base/dist/configparser");

function dosub(v) {
  if (typeof v === 'string') {
    const now = new Date();
    let hours = now.getHours() % 12;
    return v.replace(/__TIME_IN_WORDS__/g, `${TimeInWords(hours ? hours : 12, now.getMinutes())}`);
  }
  else {
    return v;
  }
}

Object.defineProperty(ConfigParser.UrlObject.prototype, 'body', {
  get() {
    return dosub(this._body);
  },
  set(v) {
    this._body = v;
  }
});

Object.defineProperty(ConfigParser.UrlObject.prototype, 'url', {
  get() {
    return dosub(this._url);
  },
  set(v) {
    this._url = v;
  }
});

module.exports = function(){}
